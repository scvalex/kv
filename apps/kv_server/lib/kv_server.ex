defmodule KVServer do
  require Logger

  def accept(port) do
    {:ok, socket, port} = loop_listen(port, 10)
    Logger.info("Accepting connections on port #{port}")
    loop_acceptor(socket)
  end

  defp loop_listen(port, remaining) do
    case remaining do
      n when n <= 0 ->
        {:error, :eaddrinuse}

      _ ->
        case :gen_tcp.listen(port, [:binary, packet: :line, active: false, reuseaddr: true]) do
          {:ok, socket} ->
            {:ok, socket, port}

          {:error, :eaddrinuse} ->
            loop_listen(port + 1, -1 + remaining)
        end
    end
  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid} = Task.Supervisor.start_child(KVServer.TaskSupervisor, fn -> serve(client) end)
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end

  def serve(socket) do
    msg =
      with {:ok, data} <- read_line(socket),
           {:ok, command} <- KVServer.Command.parse(data),
           do: KVServer.Command.run(command)

    write_line(socket, msg)
    serve(socket)
  end

  defp read_line(socket) do
    :gen_tcp.recv(socket, 0)
  end

  defp write_line(socket, {:ok, text}) do
    :gen_tcp.send(socket, text)
  end

  defp write_line(socket, {:error, :unknown_command}) do
    :gen_tcp.send(socket, "UNKNOWN COMMAND\r\n")
  end

  defp write_line(_, {:error, :closed}) do
    exit(:shutdown)
  end

  defp write_line(socket, {:error, :not_found}) do
    :gen_tcp.send(socket, "NOT FOUND\r\n")
  end

  defp write_line(socket, {:error, error}) do
    :gen_tcp.send(socket, "ERROR\r\n")
    exit(error)
  end
end
