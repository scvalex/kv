defmodule KV.BucketTest do
  use ExUnit.Case, async: true

  setup do
    bucket = start_supervised!(KV.Bucket)
    %{bucket: bucket}
  end

  test "stores values by key", %{bucket: bucket} do
    item = "milk"
    assert KV.Bucket.get(bucket, item) == nil
    KV.Bucket.put(bucket, item, 3)
    assert KV.Bucket.get(bucket, item) == 3
    assert KV.Bucket.delete(bucket, item) == 3
    assert KV.Bucket.get(bucket, item) == nil
  end

  test "buckets are temporary" do
    assert :temporary == Supervisor.child_spec(KV.Bucket, []).restart
  end
end
