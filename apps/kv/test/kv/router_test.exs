defmodule KV.RouterTest do
  use ExUnit.Case

  setup_all do
    current = Application.get_env(:kv, :routing_table)

    [node1, node2] = LocalCluster.start_nodes("my-cluster", 2)

    Application.put_env(:kv, :routing_table, [
      {?a..?m, node1},
      {?n..?z, node2}
    ])

    on_exit(fn ->
      Application.put_env(:kv, :routing_table, current)
      LocalCluster.stop()
    end)
  end

  test "route requests across nodes" do
    assert KV.Router.route("hello", Kernel, :node, []) !=
             KV.Router.route("world", Kernel, :node, [])
  end

  test "raises on unknown entries" do
    assert_raise RuntimeError, ~r/could not find entry/, fn ->
      KV.Router.route(<<0>>, Kernel, :node, [])
    end
  end
end
