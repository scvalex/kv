defmodule KV.SupervisorTest do
  use ExUnit.Case, async: true

  setup do
    %{}
  end

  test "registry is restarted" do
    name = "shopping"
    KV.Registry.create(KV.Registry, name)
    assert :error != KV.Registry.lookup(KV.Registry, name)
    ref = Process.monitor(KV.Registry)
    GenServer.stop(KV.Registry, :shutdown)
    assert_receive {:DOWN, ^ref, :process, _, :shutdown}
    :timer.sleep(100)
    assert :error == KV.Registry.lookup(KV.Registry, name)
  end
end
