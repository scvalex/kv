defmodule KV.RegistryTest do
  use ExUnit.Case, async: true

  setup context do
    _ = start_supervised!({KV.Registry, name: context.test})
    %{registry: context.test}
  end

  test "spawns buckets", %{registry: registry} do
    name = "shopping"
    assert :error == KV.Registry.lookup(registry, name)
    KV.Registry.create(registry, name)
    assert {:ok, bucket} = KV.Registry.lookup(registry, name)
    item = "milk"
    KV.Bucket.put(bucket, item, 1)
    assert 1 == KV.Bucket.get(bucket, item)
  end

  test "removes buckets on exit", %{registry: registry} do
    name = "shopping"
    KV.Registry.create(registry, name)
    {:ok, bucket} = KV.Registry.lookup(registry, name)
    Agent.stop(bucket)
    # Do a call to ensure the registry has processed the DOWN message
    _ = KV.Registry.create(registry, "bogus")
    assert :error == KV.Registry.lookup(registry, name)
  end

  test "removes bucket on crash", %{registry: registry} do
    name = "shopping"
    KV.Registry.create(registry, name)
    {:ok, bucket} = KV.Registry.lookup(registry, name)
    Agent.stop(bucket, :shutdown)
    # Do a call to ensure the registry has processed the DOWN message
    _ = KV.Registry.create(registry, "bogus")
    assert :error == KV.Registry.lookup(registry, name)
  end

  test "bucket can crash at any time", %{registry: registry} do
    name = "shopping"
    KV.Registry.create(registry, name)
    {:ok, bucket} = KV.Registry.lookup(registry, name)
    Agent.stop(bucket, :shutdown)
    catch_exit(KV.Bucket.put(bucket, "milk", 3))
  end
end
